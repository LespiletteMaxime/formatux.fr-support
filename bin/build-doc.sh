#!/bin/bash
set -e
set -u
set -o pipefail

# depends : Asciidoctor-pdf
# Installation
# gem install --pre asciidoctor-pdf
# Ajouter le PATH dans votre $HOME/.bashrc :
# export PATH="$HOME/.gem/ruby/2.4.0/bin:$PATH"
# $1 = chemin vers le chemin de build
readonly DIRNAME=$(dirname $0)
readonly SOURCES=$DIRNAME/../sources/
readonly SOURCESIMG="${SOURCES}images/"
DEST=$DIRNAME/../build/
readonly WEBSITE=$DIRNAME/../siteweb/
readonly PPT="$DIRNAME/../presentations/"
readonly PPTIMG="${PPT}images/"
# Options pour asciidoctor
readonly toc_title="Table des matières"
readonly stylesdir="${SOURCES}theme/"
readonly style="asciidoctor"
readonly fonts="${stylesdir}fonts/"
  
usage="Utilisation du script : $(basename $0) [-h] [-f fichier] [-a] [-p] [-h] [-w] [-c] \n\n \
	-h : affiche cette aide\n \
	-f fichier : convertir un seul fichier \n \
       -a : convertir tous les fichiers \n \
       -p : convertir en pdf (par defaut si -h non present) \n \
       -h : convertir en html \n \
       -w : rendre public (copie dans dossier public) \n \
       -c : nettoyer les dossiers (supprimer *.html, *.pdf)
       \n \n \
       exemple : \n \
\
$(basename $0) -aphw \n\
  Convertir tous les fichiers en PDF et HTML puis copier vers le dossier public \n \
\n \
$(basename $0) -ahf foo.adoc \n \
  Convertir le fichier foo.adoc en html \n \
"

# Generation du fichier PDF
# arguments :
# $1 : le nom du fichier a convertir
function build_file {
  echo "----------------------------------------"
  echo "Debut de la compilation du fichier "
  echo "Nom du fichier : $1"
  echo "Génération du fichier PDF"
  asciidoctor-pdf -d book -t -n -S unsafe -a lang="fr" -a icons="font" -a chapter-label="Chapitre" -a encoding="utf-8" -a toc="preamble" -a part-title="Partie" -a toclevels=3 -a numbered -a sectnumlevels=2 -a showtitle="titre" -a experimental -a pdf-stylesdir="${stylesdir}" -a pdf-style="${style}" -a pdf-fontsdir="${fonts}" -a toc-title="${toc_title}" -D ${DEST} $1
  echo "Fin de la compilation du fichier "
  echo "----------------------------------------"
  echo ""
}

# Generation du fichier HTML
# arguments :
# $1 : le nom du fichier a convertir
function build_html_file {
  echo "----------------------------------------"
  echo "Debut de la compilation du fichier "
  echo "Nom du fichier : $1"
  echo "Génération du fichier HTML"
  asciidoctor -d book -t -n -S unsafe -a lang="fr" -a icons="font" -a chapter-label="Chapitre" -a encoding="utf-8" -a toc="preamble" -a part-title="Partie" -a toclevels=3 -a numbered -a sectnumlevels=2 -a showtitle="titre" -a experimental -a pdf-stylesdir="${stylesdir}" -a pdf-style="${style}" -a pdf-fontsdir="${fonts}" -a toc-title="${toc_title}" -D ${DEST} $1
  echo "Fin de la compilation du fichier "
  echo "----------------------------------------"
  echo ""
}

# Test la presence d'au moins un argument !
if [ $# -eq 0 ]
  then
    echo -e "$usage" >&2
    exit 1
fi

# Initialisation des variables
what=""
pdf=""
html=""
public=""

while getopts 'hapwcf:' OPTION;
do
  case "$OPTION" in
    f)
      fichier="$OPTARG";
      what="file";
      ;;
    a)
      what="all"
      # Compiler tous les fichiers
      ;;
    p)
      pdf="yes"
      # Compiler en pdf
      ;;
    h)
      html="yes"
      # Compiler en html
      ;;
    w)
      # Rendre public
      public="yes"
      DEST=$DIRNAME/../public/
      ;;
    c)
      # clean 
      # TODO
      ;;
    ?)
      echo "script usage: $(basename $0) [-l] [-h] [-a arg]" >&2
      exit 1
      ;;
  esac
done

shift "$(($OPTIND -1))"

if test "$what" = "all"
then
  # Imprimer tous les documents
  FILES=$(find $SOURCES -name *.adoc)
  for file in $FILES
  do
    if test "$html" = "yes"
    then
      # Impression en html"
      if test "$pdf" = "yes"
      then
  	# Impression en HTML et en PDF
        build_file $file
        build_html_file $file
      else
        # Impression en HTML seul
        build_html_file $file
      fi
    else
      # Impression en PDF seul
      build_file $file
    fi
  done
else
  # Imprimer un seul fichier
  if test "$html" = "yes"
  then
    if test "$pdf" = "yes"
    then
      # Impression en HTML et en PDF
      build_file $fichier
      build_html_file $fichier
    else
      # Impression en HTML seul
      build_html_file $fichier
    fi
  else
    # Impression en PDF seul
    build_file $fichier
  fi
fi	
if test "$public" = "yes"
# Copier les fichiers dans le dossier public
then
  FILES=$(find $WEBSITE -name *.adoc)
  for file in $FILES
  do
    echo "Compilation du fichier ${file} en HTML"
    asciidoctor -D $DEST ${file}
  done
  mkdir $DEST/images/
  cp ${SOURCESIMG}* ${DEST}/images/
  cp ${PPTIMG}* ${DEST}/images/
  cp ${PPT}*.html ${DEST}/
fi
